// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)

const myself = {
  firstName: 'Brian',
  lastName: 'Cabacungan',
  'favorite food': 'Ramen',
    bestFriend: {
      firstName: 'Elon',
      lastName: 'Musk',
      'favorite food': 'Cereal'
    }
  };



// 2. console.log best friend's firstName and your favorite food


console.log('My BFF is ' + myself.bestFriend.firstName + ' ' + myself.bestFriend.lastName);
//My BFF is Elon Musk
console.log('My favorite food is ' + myself['favorite food']);
//My favorite food is Ramen



// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X

const dash = '-';
const circle = 'O';
const X = 'X';

var twoDimensionalArray = [
    [dash, circle, dash],
    [dash, X, circle],
    [X, dash, X]];
/*
  - O -
  - X O
  X - X
*/


// 4. After the array is created, 'O' claims the top right square.
// Update that value.

const dash = '-';
const circle = 'O';
const X = 'X';

var twoDimensionalArray = [
    [dash, circle, circle],
    [dash, X, circle],
    [X, dash, X]];
/*
  - O O
  - X O
  X - X
*/


// 5. Log the grid to the console.

console.log(twoDimensionalArray[0][0] + " " +
  twoDimensionalArray[0][1] + " " +
  twoDimensionalArray[0][2] + "\n" +
  twoDimensionalArray[1][0] + " " +
  twoDimensionalArray[1][1] + " " +
  twoDimensionalArray[1][2] + "\n" +
  twoDimensionalArray[2][0] + " " +
  twoDimensionalArray[2][1] + " " +
  twoDimensionalArray[2][2]);
/*
  - O O
  - X O
  X - X
*/ 



// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test

const myEmail = 'foo@bar.baz';

function validateEmail(email)
{
    var regexp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    //checks to see the order of the string; characters in the string
    return regexp.test(email);
};

console.log(validateEmail(myEmail));
//returns true

console.log(validateEmail('email@email.com'));
//returns true

console.log(validateEmail('@email.com'));
//returns false

console.log(validateEmail('123@emailcom'));
//returns false

// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date

const assignmentDate = '1/21/2019';

var newDate = new Date(assignmentDate);

console.log(newDate);
//Mon Jan 21 2019 00:00:00 GMT-0800 (Pacific Standard Time)



// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.

const assignmentDate = '1/21/2019';

var dueDate = new Date(assignmentDate);

console.log(dueDate.setDate(dueDate.getDate() + 7));
//1548662400000
console.log(dueDate);
//Mon Jan 28 2019 00:00:00 GMT-0800 (Pacific Standard Time)


// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const year = dueDate.getFullYear();
const month = dueDate.getDay();
const day = dueDate.getDate();

const fullMonth = months[0];



// 10. log this value using console.log

console.log('<time datetime="' + year + '-' + month + '-' + day +
            '"' + fullMonth + ' ' + day + ', ' + year + '</time>');